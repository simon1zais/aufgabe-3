package comdailycodebufferSpringboottutorial.service;

import comdailycodebufferSpringboottutorial.entity.Department;
import comdailycodebufferSpringboottutorial.reposetory.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;
    private DepartmentRepository departmentRepository;
    @BeforeEach
    void setUp() {

        Department department =
                Department.builder().departmentName("IT").departmentAddress("London").departmentCode("Test-1").departmentId(1L).build();

        Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("IT")).thenReturn(department);

    }

    @Test
    @DisplayName("Bekommt daten anhand eiens Gültigen Namen")
    public void whenvalidDepartmentName_thenDepartmentShouldFound(){

        String departmentName = "IT";
        Department found = departmentService.fetchDepartmentByName(departmentName);

        assertEquals(departmentName, found.getDepartmentName());
    }
}