package comdailycodebufferSpringboottutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ErrorMessage {
    //Diese Klasse beinhaltet nur die Datenfelder, die den Jewiligen Zustand bei einem Error speichern.
    private HttpStatus status;
    private String message;
}
