package comdailycodebufferSpringboottutorial.service;

import comdailycodebufferSpringboottutorial.Error.DepartmantNotFountExeption;
import comdailycodebufferSpringboottutorial.entity.Department;

import java.util.List;

public interface DepartmentService {

    // Diese Klasse fungiert, als schnitstelle um DepartmentController mit DepartmenImpl zu verbinden.

    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentsList();

   public Department fetchDepartmentById(Long departmentId) throws DepartmantNotFountExeption;

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartment(Long departmentId, Department department);

    public Department fetchDepartmentByName(String departmentName);
}
