package comdailycodebufferSpringboottutorial.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${welcome.message}")
    private String welcomeMessage;


    //Mit RequestMapping kann über das Vallue die bestimmte Methode aufgerufen werden.
    // @RequestMapping(value = "/", method= RequestMethod.GET)

    // Ist eine verkürzte Schreibweise zu dem dafor genannten Befehl.
    @GetMapping("/")
    public String helloWorld(){

        return welcomeMessage;

    }


}
